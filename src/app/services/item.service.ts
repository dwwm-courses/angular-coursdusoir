import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Item} from "../models/item";

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  apiUrl: string = environment.apiUrl + "/api/items";

  constructor(private http: HttpClient) { }

  getAll(): Observable<Item[]>{
    return this.http.get<Item[]>(this.apiUrl);
  }

  delete(id?: number): Observable<Item>{
    return this.http.delete(this.apiUrl+'/'+id);
  }

  add(item: Item): Observable<Item>{
    return this.http.post(this.apiUrl, item);
  }

}
