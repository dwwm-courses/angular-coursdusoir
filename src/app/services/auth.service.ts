import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Route, Router} from "@angular/router";
import {User} from "../models/user";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiUrl: string = environment.apiUrl+"/auth";

  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient, private router: Router) { }

  login(user: User) {
    return this.http.post<{token: string}>(this.apiUrl, user).subscribe(data => {
      localStorage.setItem("token", data.token);
      this.router.navigate(["/list"]);
    });
  }

  getToken(): string | null{
    return localStorage.getItem("token");
  }

  logout(): void{
    localStorage.removeItem("token");
    this.router.navigate(["/login"])
  }

}
