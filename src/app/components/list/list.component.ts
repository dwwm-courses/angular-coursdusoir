import { Component, OnInit } from '@angular/core';
import {Item} from "../../models/item";
import {ItemService} from "../../services/item.service";
import {User} from "../../models/user";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {


  items: Item[] = [];
  isLoading: boolean = false;

  constructor(private itemService: ItemService) { }

  ngOnInit(): void {
    this.isLoading = true;

    this.refresh();
  }

  delete(itemId?: number): void{
    this.isLoading = true;

    this.itemService.delete(itemId).subscribe(
      data => {
        this.refresh();
      }
    )
  }

  refresh(){
    this.itemService.getAll().subscribe(
      data => {
        this.items = data;
        this.isLoading = false;
      }
    )
  }

}
