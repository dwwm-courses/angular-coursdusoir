import { Component, OnInit } from '@angular/core';
import {Item} from "../../models/item";
import {ItemService} from "../../services/item.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  item: Item = new Item();
  isLoading = false;

  constructor(private itemService: ItemService, private router: Router) { }

  ngOnInit(): void {
  }

  submitForm(): void{
    this.isLoading = true;

    this.itemService.add(this.item).subscribe(data => {
        this.router.navigate(["/list"]);
        this.isLoading = false;
    });
  }

}
