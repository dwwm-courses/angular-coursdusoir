import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListComponent} from "./components/list/list.component";
import {FormComponent} from "./components/form/form.component";
import {SigninComponent} from "./components/signin/signin.component";
import {AuthGuard} from "./guards/auth.guard";

const routes: Routes = [
  { path: '',   redirectTo: '/list', pathMatch: 'full' },
  {path: "list", canActivate: [AuthGuard], component: ListComponent},
  {path: "add", component: FormComponent},
  {path: "login", component: SigninComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
